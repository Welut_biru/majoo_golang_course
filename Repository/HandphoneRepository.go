package repository

import (
	object "majoo_golang_course/Object"
)

type HandphoneRepositoryContract interface {
	AddPhones(*object.Handphone) (string, error)
	GetPhones() []object.Handphone
}

var handphone []object.Handphone

type HandphoneRepository struct {
	object.Handphone
}

func NewHandphoneRepository(handphone object.Handphone) HandphoneRepositoryContract {
	return &HandphoneRepository{handphone}
}

func (_r *HandphoneRepository) AddPhones(hp *object.Handphone) (string, error) {
	handphone = append(handphone, *hp)

	return "Sukses", nil
}

func (_r *HandphoneRepository) GetPhones() []object.Handphone {
	return handphone
}
