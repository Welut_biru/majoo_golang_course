package main

// pointer
// func main() {
// 	// soal 1
// 	a := 10
// 	b := &a

// 	// soal 2
// 	var c int = a
// 	c += 12

// 	// soal 3
// 	fmt.Println("variabel a: ", a)
// 	fmt.Println("variabel b: ", *b+10)
// 	fmt.Println("variabel c: ", c)
// 	fmt.Println("variabel a: ", a)

// 	// soal 4 swap
// 	x := 21
// 	y := 50
// 	resultx, resulty := swap(&x, &y)
// 	fmt.Println("nilai awalan: ", x)
// 	fmt.Println("nilai akhiran: ", y)
// 	fmt.Println("nilai awalan setelah swap: ", resultx)
// 	fmt.Println("nilai awalan setelah swap: ", resulty)

// }
func swap(x, y *int) (b, a int) {
	a, b = *x, *y
	return
}
