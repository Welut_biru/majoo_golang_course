package main

// interface Kosong, case ada di cahpter 5
type Meta struct {
	TotalRecord int
	TotalPage   int
	Page        int
}

type Response struct {
	Status bool
	Data   interface{}
	Meta   Meta
}
