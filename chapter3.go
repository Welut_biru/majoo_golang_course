package main

// Struct
type Human struct {
	Name    string
	Address string
	Age     string
}

type Teacher struct {
	NIP      string
	Name     string
	Address  string
	Age      string
	Identity Ktp
}

// type Student struct {
// 	NIS      string
// 	Name     string
// 	Address  string
// 	Age      string
// 	Identity Ktp
// }

// type Ktp struct {
// 	NIK      string
// 	ImageURL string
// }

// di comment sementara karena struct dipakai di chapter 4
// func main() {
// 	human := Human{
// 		Name:    "bili",
// 		Address: "jakarta",
// 		Age:     "21",
// 	}
// 	var humanMemo *Human = &human
// 	fmt.Println("Human data = ", human)
// 	fmt.Println("Human memo = ", &humanMemo)

// 	teacher := Teacher{
// 		NIP:     "21212",
// 		Name:    "Dodi",
// 		Address: "jakarta",
// 		Age:     "21",
// 		Identity: Ktp{
// 			NIK:      "12312314234",
// 			ImageURL: "image.1",
// 		},
// 	}

// 	fmt.Println("Data guru")
// 	fmt.Println("Nama guru", teacher.Name)
// 	fmt.Println("NIP guru", teacher.NIP)
// 	fmt.Println("Alamat guru", teacher.Address)
// 	fmt.Println("usia guru", teacher.Age)
// 	fmt.Println("identitas guru:")
// 	fmt.Println("NIK guru:", teacher.Identity.NIK)

// 	student := Student{
// 		NIS:     "21212",
// 		Name:    "Dodi",
// 		Address: "jakarta",
// 		Age:     "21",
// 		Identity: Ktp{
// 			NIK:      "12312314234",
// 			ImageURL: "image.1",
// 		},
// 	}

// 	fmt.Println("Data siswa")
// 	fmt.Println("Nama siswa", student.Name)
// 	fmt.Println("NIP siswa", student.NIS)
// 	fmt.Println("Alamat siswa", student.Address)
// 	fmt.Println("usia siswa", student.Age)
// 	fmt.Println("identitas siswa:")
// 	fmt.Println("NIK siswa:", student.Identity.NIK)
// }
