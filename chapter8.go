package main

import (
	"encoding/json"
	object "majoo_golang_course/Object"
	repository "majoo_golang_course/Repository"
	"os"
)

// Polymorpism

type Oppo struct {
	object.Handphone
}

type Xiaomi struct {
	Handphone object.Handphone
}

type PolymorphismContract interface {
	SetPhones(*object.Handphone) (string, error)
	GetPhones() []object.Handphone
}

type Polymorphism struct {
	repository repository.HandphoneRepository
}

func NewPolimorphism(repo repository.HandphoneRepository) PolymorphismContract {
	return &Polymorphism{
		repository: repo,
	}
}

func (_p *Polymorphism) SetPhones(phone *object.Handphone) (string, error) {
	phones, err := _p.repository.AddPhones(phone)
	return phones, err
}

func (_p *Polymorphism) GetPhones() []object.Handphone {
	return _p.repository.GetPhones()
}
func main() {
	xiaomi := Xiaomi{
		object.Handphone{
			Name:  "xiaomi",
			Type:  "Gaming",
			Price: 25000000,
			Color: "Black campaign",
		},
	}

	oppo := Oppo{
		object.Handphone{
			Name:  "opo",
			Type:  "bussines",
			Price: 25000000,
			Color: "Black campaign",
		},
	}
	SetData(&xiaomi.Handphone)
	SetData(&oppo.Handphone)

	data := PolymorphismContract.GetPhones(NewPolimorphism(repository.HandphoneRepository{}))
	resp := NewResponse(data, len(data))
	response, err := json.Marshal(*resp)
	if err != nil {
		panic("error gaes")
	}
	os.Stdout.Write(response)

}

func SetData(object *object.Handphone) {
	PolymorphismContract.SetPhones(NewPolimorphism(repository.HandphoneRepository{}), object)
}

func NewResponse(data interface{}, tr int) *object.Response {
	return &object.Response{
		Status: false,
		Data:   data,
		Meta: object.Meta{
			TotalRecord: tr,
			TotalPage:   1,
			Page:        1,
		},
	}
}
