package main

import (
	"fmt"
)

// Method
// func main() {
// 	student := Student{
// 		NIS:     "21212",
// 		Name:    "Dodi",
// 		Address: "jakarta",
// 		Age:     "21",
// 		Identity: Ktp{
// 			NIK:      "12312314234",
// 			ImageURL: "image.1",
// 		},
// 	}
// 	student.GetStudent()

// 	student.SetAddress()

// 	student.GetStudent()

// }

// karena sifatnya perlu melakukan perubahan pada isi attribute address maka perlu dipakaikan pointer
func (student *Student) SetAddress() {

	student.Address = "Jalani saja hidupmu jangan kebanyakan pamrih karena pamrih itu tidak baik"

}

// tidak menggunakan pointer dikarenakan tidak perlu melakukan perubahan pada data
func (student Student) GetStudent() {

	// std.Name =
	fmt.Println("Data siswa")
	fmt.Println("Nama siswa", student.Name)
	fmt.Println("NIP siswa", student.NIS)
	fmt.Println("Alamat siswa", student.Address)
	fmt.Println("usia siswa", student.Age)
	fmt.Println("identitas siswa:")
	fmt.Println("NIK siswa:", student.Identity.NIK)

}

type Student struct {
	NIS      string
	Name     string
	Address  string
	Age      string
	Identity Ktp
}

type Ktp struct {
	NIK      string
	ImageURL string
}
