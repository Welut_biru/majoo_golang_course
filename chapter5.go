package main

// import (
// 	"encoding/json"
// 	"fmt"
// 	Object "majoo_golang_course/Object"
// 	"os"
// )

// // Interface
// type BangunDatarContract interface {
// 	Luas() int
// 	Keliling() int
// }

// func main() {
// 	var persegi Persegi = *NewBangunDatar(725)

// 	luas := persegi.Luas()
// 	fmt.Println("Luas bangun datar adalah: ", luas)

// 	keliling := persegi.Keliling()
// 	fmt.Println("Keliling bangun datar adalah: ", keliling)

// 	Object.SetBook(1, "memuja kasih", "2121")
// 	Object.SetBook(2, "memuja cinta", "2345")
// 	Object.SetBook(3, "memuja Aura", "6565")

// 	book := Object.GetBook()

// 	resp := NewResponse(book, len(book))
// 	response, err := json.Marshal(*resp)
// 	if err != nil {
// 		panic("weeoe gaes")
// 	}
// 	os.Stdout.Write(response)
// }

// func NewResponse(data interface{}, tr int) *Object.Response {
// 	return &Object.Response{
// 		Status: false,
// 		Data:   data,
// 		Meta: Object.Meta{
// 			TotalRecord: tr,
// 			TotalPage:   1,
// 			Page:        1,
// 		},
// 	}
// }

// // func (*Object.Response) GetDataResponse() {
// // 	fmt.Println(Object.Response)
// // }

// func NewBangunDatar(sisi int) *Persegi {
// 	return &Persegi{
// 		Sisi: sisi,
// 	}
// }

// func (persegi Persegi) Keliling() int {
// 	return persegi.Sisi * 4
// }

// func (persegi Persegi) Luas() int {
// 	return persegi.Sisi * persegi.Sisi
// }

// type Persegi struct {
// 	Sisi int
// }
