package object

func SetBook(id int, name, kode string) {
	book := new(book)
	book.ID = id
	book.Nama = name
	book.Kode = kode

	bk = append(bk, *book)
	// fmt.Println(*bk)

}

func GetBook() []book {
	return bk
}

var bk []book

type book struct {
	ID   int    `json:"id"`
	Nama string `json:"nama"`
	Kode string `json:"kode"`
}
