package object

type Handphone struct {
	Name  string
	Type  string
	Price int64
	Color string
}
