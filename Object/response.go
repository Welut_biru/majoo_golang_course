package object

type Meta struct {
	TotalRecord int `json:"total_record"`
	TotalPage   int `json:"total_page"`
	Page        int `json:"page"`
}

type Response struct {
	Status bool        `json:"status"`
	Data   interface{} `json:"data"`
	Meta   Meta        `json:"meta"`
}
