package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

type Transaction struct {
	NoAntre      int
	IdTransaksi  int
	KodeProduksi string
	CustomerName string
	Qty          int
}

var data []Transaction
var cust = []string{
	"Doni", "Kasina", "Asyrofie", "Mursalin", "Bambang", "Ridho", "Saikoji",
}

type Tr []Transaction

func (a Tr) Len() int           { return len(a) }
func (a Tr) Less(i, j int) bool { return a[i].NoAntre < a[j].NoAntre }
func (a Tr) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

func main() {

	done := make(chan bool)
	ticker := time.NewTicker(time.Second)

	go func() {
		fmt.Println("Menunggu transaksi masuk... (ditandai dengan bintang)")
		fmt.Println("inputkan segala tombol untuk ambil daftar transaksi terkini:")
		var a string
		fmt.Scan(&a)
		done <- true
	}()

	for {
		select {
		case <-done:
			ticker.Stop()
			fmt.Println("============================================================================")
			fmt.Println("List data transaksi yang masuk sebelum di urutkan berdasarkan nomor antrean:")
			fmt.Println("============================================================================")
			Cetak(data)

			fmt.Println("============================================================================")
			fmt.Println("List data transaksi yang masuk sesudah di urutkan berdasarkan nomor antrean:")
			fmt.Println("============================================================================")
			sort.Sort(Tr(data))
			Cetak(data)
			fmt.Println("============================================================================")
			return

		case <-ticker.C:

			channel_transact := make(chan []Transaction, 5)
			go SendMessage(channel_transact, GenerateData())

			PrintMessage(channel_transact)
		}

	}
}

func GenerateData() Transaction {
	data := new(Transaction)
	data.NoAntre = rand.Intn(10)
	data.IdTransaksi = rand.Intn(20)
	data.KodeProduksi = RandomString(10)
	data.Qty = rand.Intn(5)
	data.CustomerName = cust[rand.Intn(6)]

	return *data
}

func SendMessage(ch chan<- []Transaction, tr Transaction) {

	for i := 0; i < 2; i++ {
		temp_data := new(Transaction)
		temp_data.NoAntre = tr.NoAntre + i
		temp_data.IdTransaksi = tr.IdTransaksi + i
		temp_data.KodeProduksi = tr.KodeProduksi
		temp_data.Qty = tr.Qty + i
		temp_data.CustomerName = tr.CustomerName

		data = append(data, *temp_data)
	}

	ch <- data
	close(ch)
}

func PrintMessage(ch <-chan []Transaction) {
	fmt.Print(" * ")
}

func Cetak([]Transaction) {
	for i := range data {
		fmt.Println("no_antre", data[i].NoAntre, ", Id_transaksi:", data[i].IdTransaksi, ", Nama Pelanggan:", data[i].CustomerName, ", kode_produk:", data[i].KodeProduksi, ", qty:", data[i].Qty)
	}
}

func RandomString(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}
